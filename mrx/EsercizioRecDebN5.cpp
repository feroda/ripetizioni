#include <iostream>
using namespace std;

const int DIM = 10;
int V[DIM];
int value_to_search;

void CaricaArray(int V[], int dim);
void StampaArray(int V[], int dim);
void OrdinaArray(int V[], int N);
bool RicercaDic(int V[], int N, int X);


int main()
{
    cout << "Caricamento:" << endl;
    CaricaArray(V, DIM);
    cout << endl;
    cout << "Stampa:" << endl;
    StampaArray(V, DIM);
    cout << endl;
    OrdinaArray(V, DIM);
    cout << endl;
    cout << "Array ordinato:" << endl;
    StampaArray(V, DIM);
    cout << endl;
    cout << "Quale numero vuoi cercare con la ricerca dicotomica?" << endl;
    cin >> value_to_search;
    RicercaDic(V, DIM, value_to_search);
}

void CaricaArray(int V[], int dim) {
    for(int i=0; i<dim; i++){
        cin >> V[i];
    }
}

void StampaArray(int V[], int dim) {
    for(int i=0; i<dim; i++){
        cout << i << ") " << V[i] << endl;
        
    }
} 

void scambia(int *a, int *b) {
    int temp;
    temp = *a;
    *a = *b;
    *b = temp;
}

void OrdinaArray(int V[], int N) {
    int i, j;
    for(i=0; i<N-1; i++){
        for(j=0; j<N-i-1; j++){
            if( V[j] > V[j+1] ){
                scambia(&V[j], &V[j+1]);
            }
        }
    }
}


bool RicercaDic(int V[], int N, int X){
    int sx, dx, md;
    sx=0;
    dx=N-1;
    bool trovato = false;
    do{
        md = (sx+dx)/2;
        if(V[md] == X){
            trovato = true;
        }else if(V[md] < X){
            sx = md+1;
        }else{
            dx = md-1;
        }
    }while(trovato == false && sx <= dx);
    
    if (trovato) {
        cout << "Il numero " << X << " e' stato trovato in posizione " << md << endl;
    } else {
        cout << "Il numero " << X << " non e' stato trovato" << endl;
    }
    return trovato;
    
}
