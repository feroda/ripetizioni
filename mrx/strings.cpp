#include <string>
#include <iostream>

using namespace std;

int main() {

    string mystring;
    cout << endl << "Inserisci una stringa: ";
    cin >> mystring;

    cout << "La lunghezza della stringa inserita e': " << mystring.length() << endl;

    cout << "Il carattere in posizione 0 e': " << mystring.at(0) << endl;
    cout << "Il carattere in posizione 3 e': " << mystring.at(3) << endl;
}
