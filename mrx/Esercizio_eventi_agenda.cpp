#include <iostream>
#include <string>

using namespace std;

const int maxEventi = 3;

struct s_evento {
    string nome;
    struct s_data {
        int gg, mm, aa;
		int D;
    } Data;
} Evento[maxEventi];

struct s_data D;
int nEventi = 0;


void Menu();
void Inserisci_evento();
void Visualizza_eventi();
void Ricerca_eventi();



int scelta;

int main()
{
    do{
        Menu();
        switch(scelta) {
            case 1: Inserisci_evento(); break;
            case 2: Visualizza_eventi(); break;
            case 3: Ricerca_eventi(); break;
        }
        
    } while(scelta != 0);
    

    return 0;
}

void Menu() {
    cout << " --- Menu --- " << endl;
    cout << "1) Inserisci un nuovo evento" << endl;
    cout << "2) Visualizza gli eventi registrati" << endl;
    cout << "3) Ricerca eventi" << endl;
    cout << "Scegli" << endl;
    cin >> scelta;
    
}

void Inserisci_evento() {
    s_evento ev;
    
    cout << "Inserimento evento" << endl;
    if (nEventi == maxEventi){
        cout << "Tabella piena" << endl;
        return;
    }
    
    cout << "Nome: " << endl;
    getline(cin, Evento[nEventi].nome);
    cout << "Data (gg/mm/aa)" << endl;
    cin >> Evento[maxEventi].Data.gg >> Evento[maxEventi].Data.mm >> Evento[maxEventi].Data.aa;
    nEventi ++;
    return;
    
    
}


int gData(struct s_data D){
    return D.gg + (D.mm *30) + (D.aa *365);
}


void Visualizza_eventi() {
	s_evento ev;
    int i, k;
    for(i=0; i<nEventi; i++){
        if(gData (ev.Data) > gData(Evento[i].Data)){
            break;
        }
    }
    
    for(k=nEventi -1; k>=i ;k--){
        Evento[k+1] = Evento[k];
    }
    Evento[i] = ev;

    nEventi ++;
    
    
}

void Ricerca_eventi() {
    struct s_data D;
    cout << "Inserisci data" << endl;
    cin >> D.gg >> D.mm >> D.aa;
    
    int i = 0;
    bool trovato = false;
    while(trovato == false && i<nEventi);
    {
        if(Evento[i].Data.gg == D.gg && Evento[i].Data.mm == D.mm && Evento[i].Data.aa == D.aa){
            trovato = true;
        }else{
            i++;
        }
    }
    
    
}



