#include <iostream>
using namespace std;

const int DIM = 8;
int V[DIM];

void caricaArray(int V[], int dim);
void stampaArray(int V[], int dim);
void stampaArrayPari(int V[], int dim);
void stampaArrayDispari(int V[], int dim);

int main()
{
    
    cout << "Caricare gli elementi:" << endl << endl;
    caricaArray(V, DIM);
    cout << endl << endl;
    
    cout << "Elementi dell'array:" << endl << endl;
    stampaArray(V, DIM);
    
    cout << endl << endl;
    
    cout << "Elementi dell'array con indice pari:" << endl << endl;
    stampaArrayPari(V, DIM);
    
    cout << endl << endl;
    
    cout << "Elementi dell'array con indice dispari:" << endl << endl;
    stampaArrayDispari(V, DIM);

    return 0;
}

void caricaArray(int V[], int dim) {
    for(int i=0; i<dim; i++){
        /* qui un cout lo aggiungerei per dire all'utente cosa deve inserire */
        //ad esempio cout << "Inserire numero con indice " << i << ": ";
        cin >> V[i];
    }
}
void stampaArray(int V[], int dim) {
    for(int i=0; i<dim; i++){
        cout << i << ") " << V[i] << endl;
    }
}

void stampaArrayPari(int V[], int dim) {
    for(int i=0; i<dim; i+=2){
        cout << i << ") " << V[i] << endl;
    }
}

void stampaArrayDispari(int V[], int dim) {
    for(int i=1; i<dim; i+=2){
        cout << i << ") " << V[i] << endl;
    }
    
}
