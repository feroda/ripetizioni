#include <iostream>
#include <string>
#include <iomanip>

using namespace std;

const int DIM = 5;
string V[DIM];
string nome;
int c;

void StampaNomi(string V[], int dim);
void OrdinaNomi(string V[], int dim);
void Scambia(string *a, string *b);

int main() {
    for (c=0; c<DIM; c++) {
        cout << "Inserisci nome: ";
        cin >> V[c];
    }

    OrdinaNomi(V, DIM);
    StampaNomi(V, DIM);   

    return 0;
}

void StampaNomi(string V[], int dim) {
    
    for (int i=0; i<dim; i++) {
        cout << setw(2) << i+1 << ") " << V[i] << endl;
    }
}

void Scambia(string *a, string *b) {
    string tmp = *a;
    *a = *b;
    *b = tmp;
}

void OrdinaNomi(string V[], int dim) {

    int i, j;
    for (i=0; i<dim-1; i++) {
        for (j=0; j<dim-i-1; j++) {
            if (V[j] > V[j+1]) {
                Scambia(&V[j+1], &V[j]);
            }
        }
    }
}

/*

feroda@daitarn3:~/src/test/ripetizioni/mrx$ g++ ordina_nomi.cpp 
feroda@daitarn3:~/src/test/ripetizioni/mrx$ ./a.out 
Inserisci nome: Cielcio
Inserisci nome: Bruno
Inserisci nome: Riccardo
Inserisci nome: Luca
Inserisci nome: Max
 1) Bruno
 2) Cielcio
 3) Luca
 4) Max
 5) Riccardo
*/
