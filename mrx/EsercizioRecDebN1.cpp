#include <iostream>
using namespace std;

const int DIM= 5;
int V[DIM];

void caricaVettore(int V[], int dim);
void StampaVettore(int V[], int dim);
void StampaVettoreInverso(int V[], int dim);

int main()
{
    cout << "CARICAMENTO VETTORE" << endl << endl;
    caricaVettore(V, DIM);
    cout << endl << "VISUALIZZAZIONE VETTORE" << endl << endl;
    StampaVettore(V, DIM);
    cout << endl << "VISUALIZZAZIONE VETTORE INVERSO" << endl << endl;
    StampaVettoreInverso(V, DIM);
    
    
    system("pause");
}

void caricaVettore(int V[], int dim){
    int i;
    for(i=0; i<dim; i++){
        cout << "Inserisci numero: ";
        cin >> V[i];
    }
}


void StampaVettore(int V[], int dim){
    int i;
    for(i=0; i<dim; i++){
        cout << i << ")  " << V[i] << endl;
    }
}

void StampaVettoreInverso(int V[], int dim){
    int i;
    for(i=9; i>=0; i--){
        cout << i << ")  " << V[i] << endl;
    }
}




