/*
# Copia di un file di testo in un altro file

1. Apertura del file origine in lettura
2. Leggere il contenuto del file e salvarlo in una variabile (stringa)
3. Chiusura del file origine

4. Apertura del file destinazione in scrittura
5. Scrivere il contenuto della variabile stringa nel file di destinazione
6. Chiusura del file destinazione

## Più in dettaglio

0. Dichiaro:
- fname_origin di tipo stringa = nome del file sorgente;
- fname_dest di tipo stringa = nome del file destinazione;
- content di tipo stringa;

1. Apro un file esistente con modo "read" e ottengo l'handle `sr`
2. Leggo il contenuto di `sr` e lo salvo in una variabile stringa `content`
3. Chiudo l'handle `sr`

4. Apro un nuovo file in modo "write" e ottengo l'handle `sw`
5. Scrivo il contenuto della variabile `content` nel contenuto di `sw`
6. Chiudo l'handle `sw`
*/

using System;
using System.IO;
using System.Text;

class FileCopy {

    public static void Main() {

        string fname_origin = "file_testo.txt";
        string fname_dest = "file_dest.txt";
        string content = "";

        if (File.Exists(fname_origin)) {
            using (StreamReader sr = new StreamReader(fname_origin)) {
                // content = sr.ReadToEnd();
                string line;
                while ((line = sr.ReadLine()) != null) {
                    content += line + "\n";
                } 
            }

            using (StreamWriter sw = new StreamWriter(fname_dest)) {
                sw.WriteLine(content);
            }

        } else {
            Console.WriteLine("Il file origine " + fname_origin + " non esiste");
        }
    }
}
