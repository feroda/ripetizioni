#include <iostream>
#include <string>
#include <iomanip>
using namespace std;
string Par(string);

struct Studente { string nome; string cognome;
                int anno; string comune; int classe;
                string sezione;
};

Studente student1;


int main()
{
    student1.nome = Par("nome");
    student1.cognome = Par("cognome");
    student1.anno = stoi(Par("anno"));
    student1.comune = Par("comune");
    student1.classe = stoi(Par("classe"));
    student1.sezione = Par("sezione");
    
    cout << endl;
    cout << endl;
    
    cout << setw(25) << setfill(':') <<" SCHEDA STUDENTE "  << right << setfill(':') << right << endl;
    cout << "Nome: " << student1.nome;
    cout << setw(10) << setfill(' ') << "Cognome: " << setw(20) << student1.cognome << right << endl;
    cout << "Anno: " << student1.anno;
    cout << setw(10) << setfill(' ') << "Città: " << setw(20) << student1.comune << endl;
    cout << "Classe: " << student1.classe << student1.sezione << endl;
    
    
    
}

string Par(string name){
    string value;
    cout << "Inserisci " << name << ": ";
    cin >> value;
    return value;
}
