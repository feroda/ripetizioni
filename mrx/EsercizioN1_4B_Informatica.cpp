#include <iostream>
#include <string.h>

using namespace std;


struct s_match { string teamC; 
                 string teamO;
                 int goal1;
                 int goal2;
                 
                 
};


void CaricaDati(s_match P[], int dim);
void StampaDati(s_match P[], int dim);


const int DIM = 2;
s_match P[DIM];

int main(){
    cout << "Caricamento dati partita" << endl; 
    CaricaDati(P, DIM);
    cout << endl;
    cout << "Stampa dei dati partita" << endl;
    StampaDati(P, DIM);
    
    return 0;
}

void CaricaDati(s_match P[], int dim){

    for(int i=0; i<dim; i++){
        cout << "\nPARTITA " << i+1 << endl;
        cout << "Squadra di casa: " << endl;
        cin >> P[i].teamC;
        cout << "Goal: ";
        cin >> P[i].goal1;
        cout << endl;
        cout << "Squadra ospite: " << endl;
        cin >> P[i].teamO;
        cout << "Goal: ";
        cin >> P[i].goal2;
        cout << endl;
    }
}


void StampaDati(s_match P[], int dim){

    for(int i=0; i<dim; i++){
        
        cout << "\nPARTITA " << i+1 << endl;
        cout << "Squadra di casa: \n" << P[i].teamC;
        cout << "\tGoal: " << P[i].goal1 << endl;
        
        cout << "Squadra ospite: \n" << P[i].teamO;
        cout << "\tGoal: " << P[i].goal2;
        cout << endl;
        
        if(P[i].goal1 > P[i].goal2){
            cout << P[i].teamC << " V" << endl; 
        }else{
            cout << P[i].teamO << " V" << endl;
        }
    
    }
    
}
