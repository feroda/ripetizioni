#include <iostream>
#include <string>
#include <iomanip> 
using namespace std;

const int DIM = 5;
struct Info { string name;  string sur; int age;  int nBooks; };
Info book[DIM];

void StampaTabella( Info book[], int dim);
void OrdinaBooks(Info book[], int dim);
void scambia(Info *a, Info *b);

int main() {
    for (int i=0; i<DIM; i++) {
        cout << "Inserisci nome: ";
        cin >> book[i].name;
        cout << "Inserisci cognome: ";
        cin >> book[i].sur;
        cout << "Inserisci eta': ";
        cin >> book[i].age;
        cout << "Inserisci numero di libri: ";
        cin >> book[i].nBooks;
    }
    cout << "Hai inserito: " << endl;
    StampaTabella( book, DIM);
    OrdinaBooks( book, DIM);
    cout << "Ed ecco i libri ordinati per numero di libri scritti: " << endl;
    StampaTabella(book, DIM);
}

void scambia(Info *a, Info *b){
    Info temp;
    temp = *b;
    *a=*b;
    *b=temp;
}

void StampaTabella( Info book[], int dim) {
    int i;
    for(i=0; i<dim; i++){
        cout << book[i].name << " " << book[i].sur << " anni " << book[i].age << " libri scritti " << book[i].nBooks << endl;
       
    }
}

void OrdinaBooks(Info book[], int dim){
    int i, j;
    for(i=0; i<dim-1; i++){
        for(j=0; j<dim-i-1; j++){
            if(book[j].nBooks>book[j+1].nBooks){
                scambia(&book[j], &book[j+1]);
            }
        }
    }
}
