#include <stdio.h>

//void stampaArray(int[], int);
void stampaArray(int V[], int dim) {
    int i;
    for (i=0; i<dim; i++) {
        printf("%i\n", V[i]);
    }
}

void caricaArray(int V[], int dim) {
    int i;
    for (i=0; i<dim; i++) {
        scanf("%i", &dim);
    }
}

void caricaArray2(int V[], int dim) {
    /*
    Riempie l'array V di dimensione dim con i valori inseriti in input dall'utente.
    */
    int i;
    for (i=0; i<dim; i++) {
        scanf("%i", &V[i]);
        printf("In posizione %i l'array vale: %i\n", i, V[i]);
        printf("V[%i]=%i\n\n", i, V[i]);
    }

    stampaArray(V, dim);
}


void main() {
    int dim = 4;
    int V[dim];
    caricaArray2(V, dim);
}
