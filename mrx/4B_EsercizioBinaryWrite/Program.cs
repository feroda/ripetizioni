using System;
using System.IO;
using System.Text;

class Program{
    static void Main (string[] arg){
        string fileName = "file.bin";
        string autore = "Stephen King";
        string titolo = "IT";
        double prezzo = 40.30;

        using (FileStream fs = File.Open(fileName, FileMode.Append)) {
            using (BinaryWriter bw = new BinaryWriter(fs)) {
                bw.Write(autore);
                bw.Write(titolo);
                bw.Write(prezzo);
            }
        }

        using (FileStream fs = File.Open(fileName, FileMode.Open)) {
            using (BinaryReader br = new BinaryReader(fs)) {
                int i = 0;
                while (br.PeekChar() != -1) {
                    Console.Write(i +") A:" + br.ReadString());
                    Console.Write(" T:" + br.ReadString());
                    Console.WriteLine(" EUR:" + br.ReadDouble());
                    i++;
                }
            }
        }
    }
}

    
