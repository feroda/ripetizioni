#include <iostream>
using namespace std;

//Esercizio: scambiare il primo e l'ultimo elemento di un array

//Appunti per la risoluzione:
//Array = V
//Primo elemento dell'array = V[0]
//Ultimo elemento dell'array = V[4]

void scambia(int *a, int *b) {
    int temp;

    temp = *a;
    *a = *b;
    *b = temp;
}

void stampaArray(int V[], int dim) {

    cout << "Array: ";
    for (int i=0; i<dim; i++)
        cout << V[i] << " ";
    cout << endl;
}

const int DIM = 5;
int V[DIM] = {9,1,10,9,7};
char key;


int main() {
    cout << "Il programma scambia il primo e l'ultimo elemento del seguente array" << endl;
    stampaArray(V, DIM);

    //scambiare V[0] e V[4]
    scambia(&V[0], &V[4]);

    cout << "Dopo lo scambio l'array contiene" << endl;
    stampaArray(V, DIM);

    cout << "Premi un tasto per continuare..." << endl;
    cin >> key;
}
