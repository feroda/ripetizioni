#include <iostream>
using namespace std;

void scambia(int*a, int*b);
void Carica(int V[], int dim);
void Stampa(int V[], int dim);
void SelectionSort(int V[], int dim);


const int DIM = 10;
int V[DIM];

int main()
{
    cout << "Array caricato dall'utente" << endl;
    cout << endl;
    Carica(V, DIM);
    cout << endl << endl;
    cout << "Elementi stampati normalmente" << endl;
    Stampa(V, DIM);
    cout << endl << endl;
    SelectionSort(V, DIM);
    cout << endl << endl;
    cout << "Visualizzazione elementi ordinati" << endl;
    Stampa(V, DIM);
    
    
}

void Carica(int V[], int dim){
    for(int i=0; i<dim; i++){
        cin >> V[i];
    }
}

void Stampa(int V[], int dim){
    for(int i=0; i<dim; i++){
        cout << i << ") " << V[i] << endl;
    }
    
}

void scambia(int*a, int*b){
    int temp;
    temp = *a;
    *a = *b;
    *b = temp;
}

void SelectionSort(int V[], int dim){
    int i, j;
    int k;
    for(i=0; i<dim; i++){
        k=i;
        for(j=0; j<dim; j++){
            if(V[k]>V[j]){
               k=j; 
               scambia(&V[i], &V[k]);
            }  
        }
    }
}
