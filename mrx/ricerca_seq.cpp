#include <iostream>
using namespace std;

// Dato un array di 7 elementi, visualizzarlo,
// richiedere all'utente un numero 

void stampaArray(int V[], int dim);
void RicercaSeq(int V[], int dim, int x);

const int DIM = 7;
int V[DIM] = {26, 8, 50, 12, 19, 39, 33};
int n;

int main() {

    stampaArray(V, DIM);
    cout << "Inserisci un valore intero: ";
    cin >> n;
    cout << endl;
    RicercaSeq(V, DIM, n);
}

void stampaArray(int V[], int dim) {

    cout << "Array" << endl;

    for (int i=0; i<dim; i++) {
        cout << V[i] << " ";
    }

    cout << endl;
}


void RicercaSeq(int V[], int dim, int x) {

    int count = 0;
    for (int i=0; i<dim; i++) {
        if (V[i] == x) {
            count += 1;
        }
    }

    if (count > 0) {
        cout << "trovato" << endl;
    } else {
        cout << "non trovato" << endl;
    }
}
