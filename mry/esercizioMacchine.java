import java.lang.Math;

class Auto {
    private String model;
    private int km;

    //Costruttore di default
    public Auto() {
        this.model = "BMW"; 
        this.km = 0;
    }

    //Costruttore standard
    public Auto(String _model) {
        this.model = _model;
        this.km = 0;
    }

    public String toString() {
        return this.model;
    }

    public int getKm() {
        return this.km;
    }

    public void vai() {
        // this.km += 10;
        this.km += (int)(Math.random() * 10);
    }
    
}


public class esercizioMacchine {

    public static void main(String[] args) {

        System.out.println("Eseguo il programma con una " + args[0]);

        Auto mDefault = new Auto();
        Auto m1 = new Auto(args[0]);

        System.out.println("Ho comprato un'auto di modello " + mDefault);
        System.out.println("Ho comprato un'altra auto di modello " + m1);

        //System.out.println("Ho comprato un'auto di modello " + mDefault.getModello());
        //System.out.println("Ho comprato un'altra auto di modello " + m1.getModello());

        mDefault.vai();
        m1.vai();

        System.out.println("La " + mDefault + " ha fatto " + mDefault.getKm() + "km"); 
        System.out.println("La " + m1 + " ha fatto " + m1.getKm() + "km"); 

        if (mDefault.getKm() > m1.getKm()) {
            System.out.println("Ha vinto la " + mDefault);
        } else {
            System.out.println("Ha vinto la " + m1);
        }





    }
}
