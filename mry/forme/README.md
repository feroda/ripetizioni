# Forme geometriche ad oggetti


Creare un programma che sappia calcolare il perimetro e l'area di un triangolo, un rettangolo ed un quadrato.

Farlo con la programmazione ad oggetti, creando una classe per ogni forma geometrica.