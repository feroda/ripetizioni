class Recipiente{
    private int volume;
    private int contenuto;
    //OLD; private int isAperto; // 0 = chiuso; 1 = aperto
    private boolean isAperto; // false = chiuso; true = aperto
    
    //Costruttore di default
    public Recipiente() {
        this.volume = 20;
        this.contenuto = 0;
        this.isAperto = false;
    }

    //Costruttore standard
    public Recipiente(int vol) {
        this.volume = vol;
        this.contenuto = 0;
        this.isAperto = false;
    }

    public Recipiente(int vol, int cont) {
        this.volume = vol;
        this.contenuto = cont;
    }

    public void aggiungi(int qta) {
        //TODO: inserire qui la funzione che aumenta this.contenuto
        // solo se non sorpassa this.volume
        //SIMPLE VERSION: this.contenuto += qta;

        if (this.isAperto == true) {
            if (this.contenuto + qta <= this.volume) {
                this.contenuto += qta;
            } else {
                this.contenuto = this.volume;
            }
        }
    }

    public int getContenuto() {
        return this.contenuto;
    }

    public void apri() {
        this.isAperto = true;
    }
    public void chiudi() {
        this.isAperto = false;
    }
}



public class esercizioRecipiente1 {
    
    public static void main(String[] args) {
        Recipiente tinozza = new Recipiente(10);
        Recipiente silos = new Recipiente(1000);

        silos.apri();
        tinozza.apri();
        silos.aggiungi(10);
        tinozza.aggiungi(5);
        System.out.println("tinozza="+tinozza.getContenuto()+" silos="+silos.getContenuto());
        tinozza.chiudi();
        tinozza.aggiungi(100);
        silos.aggiungi(100);
        System.out.println("tinozza="+tinozza.getContenuto()+" silos="+silos.getContenuto());
    }
}
