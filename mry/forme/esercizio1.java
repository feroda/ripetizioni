class Orologio {

    private int h = 0;
    private int m = 0;

    public Orologio(int h, int m) {
        this.h = h;
        this.m = m;
    }

    public int getOra() {
        return this.h;
    }

    public int getMinuti() {
        return this.m;
    }

    public void tick() {
        this.m += 1;
        if (this.m >= 60) {
            this.m = 0;
            this.h += 1;
            if (this.h >= 24) {
                this.h = 0;
            }
        }
    }
}

public class esercizio1 {
    
    public static void main(String[] args) {
        Orologio o = new Orologio(10, 24);
        for (int i=0; i<60; i++) {
            System.out.println("Orario corrente " + o.getOra() + ":" + o.getMinuti());    
            o.tick();
        }
    }
}
