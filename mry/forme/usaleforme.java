import java.lang.Math;

class TriangoloRettangolo {

    private double b;
    private double h;

    public TriangoloRettangolo(double b, double h) {
        this.b = b;
        this.h = h;
    }

    public double area() {
        return this.b*this.h/2;
    }

    public double perimetro() {
        double ipotenusa = Math.sqrt((this.b*this.b)+(this.h*this.h));
        return this.b+this.h+ipotenusa;
    }

    public void setBase(double b) {
        if (b>0) {
            this.b = b;
        }
    }

    public double getBase() {
        return this.b;
    }
}


public class usaleforme {
    public static void main (String[] args) {
        double base = 3;
        double altezza = 4;
        TriangoloRettangolo tr = new TriangoloRettangolo(base, altezza);
        double area = tr.area();
        double perimetro = tr.perimetro();
        System.out.println("L'perimetro di questo oggetto misura " + perimetro);
        System.out.println("L'area di questo oggetto misura " + area);
    }
}
