import java.lang.Math;

public class TriangoloRettangolo {

    private double b;
    private double h;

    public TriangoloRettangolo(double b, double h) {
        this.b = b;
        this.h = h;
    }

    public double area() {
        return this.b*this.h/2;
    }

    public double perimetro() {
        double ipotenusa = Math.sqrt((this.b*this.b)+(this.h*this.h));
        return this.b+this.h+ipotenusa;
    }
}
