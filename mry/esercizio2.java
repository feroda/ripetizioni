class Recipiente {
    //Campi dell'oggetto
    private int volume; // 0 <= volume
    private int contenuto; // 0 <= contenuto <= volume
    private boolean is_aperto; // true = aperto; false = chiuso

    //Costruttore
    public Recipiente(int quantita) { // 0 <= quantita
        this.volume = quantita;
        this.contenuto = 0;
        this.is_aperto = false;
    }

    //Metodi Getter
    public int getVolume() { return this.volume; }
    public int getContenuto() { return this.contenuto; }
    public int capacita() { return this.volume-this.contenuto; }

    // Metodo Setter
    public void setContenuto(int valore) {
        this.contenuto = valore;
    }

    public void aggiungi(int quantita) { // 0 <= quantita
        if (this.is_aperto) {
            if (this.contenuto + quantita <= this.volume) {
                this.contenuto += quantita;
            } else {
                this.contenuto = this.volume;
            }
        }
    }

    public void rimuovi(int quantita) { // 0 <= quantita
        if (this.is_aperto) {
            if (this.contenuto - quantita >= 0) {
                this.contenuto -= quantita;
            } else {
                this.contenuto = 0;
            }
        }
    }

    public void apri() {
        this.is_aperto = true;
    }

    public void chiudi() {
        this.is_aperto = false;
    }
}


public class esercizio2 {

    public static void main(String[] args) {

        Recipiente lattina = new Recipiente(10);
        System.out.println("Capacita: " + lattina.capacita());
        lattina.aggiungi(3);
        System.out.println("Ho aggiunto 3, Capacita: " + lattina.capacita() + " (contenuto: " + lattina.getContenuto() + ")");
        lattina.apri();
        lattina.aggiungi(3);
        System.out.println("Ho aperto e aggiunto 3, Capacita: " + lattina.capacita() + " (contenuto: " + lattina.getContenuto() + ")");
        lattina.chiudi();
        lattina.rimuovi(7);
        System.out.println("Ho chiuso e rimosso 7, Capacita: " + lattina.capacita() + " (contenuto: " + lattina.getContenuto() + ")");
        lattina.apri();
        lattina.rimuovi(7);
        System.out.println("Ho aperto e rimosso 7, Capacita: " + lattina.capacita() + " (contenuto: " + lattina.getContenuto() + ")");
    }
}
